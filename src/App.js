import "./App.css";
import { useEffect, useState } from "react";
import WeatherIcon from "./components/weatherIcon";
import WeatherCard from "./components/weatherCard";
import { Container } from "semantic-ui-react";
import "semantic-ui-css/semantic.min.css";
import dayjs from "dayjs";

function App() {
  const [position, setPosition] = useState({
    latitude: 0,
    longitude: 0,
  });
  const timeOfDay = dayjs().format("HH") <= 18 ? "day" : "night";

  const [weather, setWeather] = useState({});

  const API_KEY = process.env.REACT_APP_API_KEY;

  useEffect(() => {
    navigator.geolocation.getCurrentPosition((pos) => {
      setPosition({
        latitude: pos.coords.latitude,
        longitude: pos.coords.longitude,
      });
    });
  }, [setPosition]);

  useEffect(() => {
    if (position.latitude && position.longitude) {
      fetch(
        `https://api.openweathermap.org/data/2.5/weather?lat=${position.latitude}&lon=${position.longitude}&appid=${API_KEY}`
      )
        .then((res) => res.json())
        .then((data) => setWeather(data))
        .catch((err) => console.log(err));
    }
  }, [API_KEY, position]);

  return (
    <Container
      // className={timeOfDay === "day" ? "App-day" : "App"}
      className="App-day"
    >
      {weather ? (
        <>
          <WeatherIcon
            condition={weather?.weather?.[0].main}
            time={timeOfDay}
          />
          <WeatherCard weather={weather} time={timeOfDay} />
        </>
      ) : null}
    </Container>
  );
}

export default App;
