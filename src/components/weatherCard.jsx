import { Dimmer, Loader } from "semantic-ui-react";
import "../assets/styles/weatherCard.css";

const WeatherCard = ({ ...props }) => {
  const { weather, time } = props;

  return (
    <div>
      {weather.name ? (
        <>
          <div className={time === "day" ? "weather-card-day" : "weather-card"}>
            <h1>{weather?.name}</h1>
          </div>
          <div>
            <h2>{weather?.weather?.[0]?.description}</h2>
          </div>
          <div>
            <h3>
              Temprature min: {(weather?.main?.temp_min - 273.15).toFixed(2)}{" "}
              Celcius /{" "}
              {((weather?.main?.temp_min * 9) / 5 - 459.67).toFixed(2)}{" "}
              Fahrenheit
            </h3>
            <h3>
              Temprature max: {(weather?.main?.temp_max - 273.15).toFixed(2)}{" "}
              Celcius /{" "}
              {((weather?.main?.temp_max * 9) / 5 - 459.67).toFixed(2)}{" "}
              Fahrenheit
            </h3>
          </div>
        </>
      ) : (
        <Dimmer active>
          <Loader />
        </Dimmer>
      )}
    </div>
  );
};
export default WeatherCard;
