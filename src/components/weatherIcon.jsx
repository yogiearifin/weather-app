import "../assets/styles/weatherIcon.css";
const WeatherIcon = ({ ...props }) => {
  const { condition, time } = props;

  const rain = (time) => {
    return (
      <div className={`${time === "day" ? "icon rainy-day" : "icon rainy"}`}>
        <div className={time === "day" ? "cloud-day" : "cloud"}></div>
        <div className={time === "day" ? "rain-day" : "rain"}></div>
      </div>
    );
  };

  const storm = (time) => {
    return (
      <div
        className={`${
          time === "day" ? "icon thunder-storm-day" : "icon thunder-storm"
        }`}
      >
        <div className="cloud"></div>
        <div className="lightning">
          <div className="bolt"></div>
          <div className="bolt"></div>
        </div>
      </div>
    );
  };

  const clearDay = () => {
    return (
      <div className={`icon sunny`}>
        <div className="sun">
          <div className="rays"></div>
        </div>
      </div>
    );
  };

  const clearNight = () => {
    return (
      <div className={`icon clear-night`}>
        <div className="moon"></div>
      </div>
    );
  };

  const cloudy = (time) => {
    return (
      <div className={time === "day" ? `icon cloudy` : `icon cloudy-day`}>
        <div className={"cloud"}></div>
        <div className={time === "day" ? "cloud-day" : "cloud"}></div>
      </div>
    );
  };

  const snow = (time) => {
    return (
      <div className={`icon flurries`}>
        <div className="cloud"></div>
        <div className="snow">
          <div className="flake"></div>
          <div className="flake"></div>
        </div>
      </div>
    );
  };

  const weatherCondition = (condition) => {
    console.log("time", time);
    switch (condition) {
      case "Clear":
        return time === "day" ? clearDay() : clearNight();
      case "Clouds":
        return cloudy(time);
      case "Rain":
        return rain(time);
      case "Drizzle":
        return snow(time);
      case "Thunderstorm":
        return storm(time);
      default:
        return false;
    }
  };

  return <div>{condition ? weatherCondition(condition) : null}</div>;
};

export default WeatherIcon;
